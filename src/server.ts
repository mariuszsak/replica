import {app} from "./index";

const port = 3000;

if (process.env.NODE_ENV !== 'test') {
    app.listen(port, () => {
        console.log('App is running.');
    });
}
